{
  "sites": [
     {
      "key": "csp_Kumao",
      "name": "🐱 酷猫",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "قط رائع"
    },
     {
      "key": "csp_NaNa",
      "name": "👒 七七",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "سبعة وسبعة"
    },
     {
      "key": "csp_DiDuan",
      "name": "🌀 低端影视",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "السينما والتلفزيون الرديئة"
    },
    {
      "key": "csp_CZSPP",
      "name": "🚬 厂长资源",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "موارد مدير المصنع"
    },
   
 
    {
      "key": "csp_Buka",
      "name": "🧼 真不卡",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "حقا ليس عالقا"
    },
    {
      "key": "csp_Auete",
      "name": "🧿 Auete",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "أويتي"
    },
    {
      "key": "csp_AliPanSou",
      "name": "🐱 喵狸盘搜",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "مواء الراكون"
    },
    {
      "key": "csp_GitCafe",
      "name": "🦊 小纸条",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "ملاحظة"
    },
{
      "key": "csp_77",
      "name": "77(SP)",
      "type": 3,
      "api": "csp_Kunyu77",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1
    },
{
      "key": "csp_Concert",
      "name": "   演唱会",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "???? ???????"
    },
    {
      "key": "csp_Animal",
      "name": "   动物世界",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 0,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "???? ???????"
    },
    {
      "key": "csp_Opera",
      "name": "   戏曲",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "?????"
    },
    {
      "key": "csp_Cokemv",
      "name": "焦炭（SP）",
      "type": 3,
      "api": "csp_Cokemv",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1
    },
{
      "key": "csp_CZSPP",
      "name": "厂长(SP)",
      "type": 3,
      "api": "csp_CZSPP",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0
    },
{
      "key": "csp_Fantuan",
      "name": "饭团(SP)",
      "type": 3,
      "api": "csp_Fantuan",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1
    },
{
      "key": "csp_Buka",
      "name": "不卡(SP)",
      "type": 3,
      "api": "csp_Buka",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1
    },
{
      "key": "csp_GitCafe",
      "name": "小纸条",
      "type": 3,
      "api": "csp_GitCafe",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0
    },
    {
      "key": "csp_AliPanSou",
      "name": "阿里盘搜",
      "type": 3,
      "api": "csp_AliPanSou",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0
    },
//{"key": "ikan_spider","name": "爱看(SP)","type": 3,"api": "csp_IKan","searchable": 1,"quickSearch": 1,   //"filterable": 1},
//{"key": "mjxq_spider","name": "美剧星球(SP)","type": 3,"api": "csp_MjxqApp","searchable": 1,"quickSearch": //1,"filterable": 1},
    {
      "key": "csp_xpath_qiyou",
      "name": "奇优电影(XP)",
      "type": 3,
      "api": "csp_XPath",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/qiyou.json"
    },
    {
      "key": "csp_xpath_cokemv",
      "name": "Cokemv(XPMac)",
      "type": 3,
      "api": "csp_XPathMacFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/cokemv.json"
    },
 {
      "key": "csp_xpath_huya",
      "name": "虎牙直播(XP)",
      "type": 3,
      "api": "csp_XPathMacFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/huya.json"
    },
{
      "key": "csp_xpath_kuqi",
      "name": "酷奇MV(XP)",
      "type": 3,
      "api": "csp_XPathMacFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/kuqimv.json"
    },    
{"key":"csp_xpath_qcys","name":"奇粹影视(XP)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":0,"filterable":1,"ext":"https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/ikuwoo.json"},
    {
      "key": "六度",
      "name": "六度(XPM)",
      "type": 3,
      "api": "csp_XPathMacFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/6dtv.json"
    },
    {
      "key": "csp_xpath_unss",
      "name": "九洲影视(XPMF)",
      "type": 3,
      "api": "csp_XPathMacFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/%E4%B9%9D%E5%B7%9E.json"
    },
{
      "key": "csp_appys_xiaogui_躺平影视",
      "name": "躺平影视(m)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "http://www.lltpys.com/api.php/app/"
    },
    {
      "key": "csp_appysv2_钟特影视",
      "name": "钟特影视(v1)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://app.zteys.com/api.php/v1.vod"
    },
 {
      "key": "csp_xpath_chen",
      "name": "尘落(优XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/chenluo.json"
    },
{"key":"csp_xpath_hmtv","name":"花猫TV(XP)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":0,"filterable":1,"ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/huamao.json"},{"key":"csp_xpath_hhys","name":"火火影视TV(XP)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":0,"filterable":1,"ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/huohuo.json"},
    {
      "key": "csp_AppYs_秒播",
      "name": "秒播",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "http://mkk.gotka.top/api.php/v1.vod"
    },
    {
      "key": "csp_appysv2_DC影视",
      "name": "DC影视(优)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "http://chaorenbb.com/api.php/v1.vod"
    },
{
      "key": "csp_appysv2_9e国语",
      "name": "9E国语",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "http://vod.9e03.com/lvdou_api.php/v1.vod"
    },   
    {
      "key": "csp_appysv2_金叶影院",
      "name": "金叶影院(M2)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "http://r.zjj.life:88/mv/api.php/Chengcheng/vod"
    },
     {
      "key": "csp_xpath_netf",
      "name": "网飞(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/netflix.json"
    },
    {
      "key": "csp_xpath_libv",
      "name": "libvio(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/libvio.json"
    },
{
      "key": "csp_xpath_saohuo",
      "name": "骚火(XPath)",
      "type": 3,
      "api": "csp_XPath",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/%E9%AA%9A%E7%81%AB.json"
    },
    {
      "key": "csp_xpath_lezhutv",
      "name": "乐猪(XPath)",
      "type": 3,
      "api": "csp_XPath",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 0,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/%E4%B9%90%E7%8C%AAtv.json"
    },
 {
      "key": "csp_xpath_ddg",
      "name": "达达龟(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/dadagui.json"
    },
    {
      "key": "csp_appys_看365",
      "name": "看365",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "https://www.kan365.xyz/api.php/app/"
    },
{
      "key": "csp_appysv2_看看",
      "name": "看看吧",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "http://888.ccboke.top/ruifenglb_api.php/v1.vod"
    },
    {
      "key": "csp_appysv2_天空影视V2",
      "name": "天空影视V2(M2)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "https://www.tkys.tv/xgapp.php/v2/"
    },
{"key":"csp_xpath_subb","name":"素白白TV(XP)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":0,"filterable":1,"ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/subaibai.json"},
{
      "key": "csp_xpath_vipdy",
      "name": "VIP电影(XP)",
      "type": 3,
      "api": "csp_XPathMacFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/vip1280.json"
    },
{"key":"csp_xpath_juhu","name":"剧荒(XP)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":0,"filterable":1,"ext":"https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/juhuang.json"},
{
      "key": "csp_xpath_huah",
      "name": "花和电影(XP)",
      "type": 3,
      "api": "csp_XPathMacFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/huahe.json"
    },
{
      "key": "csp_xpath_dsx",
      "name": "大师兄XP",
      "type": 3,
      "api": "csp_XPathMacFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/dsxys.json"
    },
{"key":"csp_xpath_mboy","name":"MBO影视(XP)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":0,"filterable":1,"ext":"https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/mbomovice.json"},
{
      "key": "csp_xpath_zjdr",
      "name": "追剧达人(XP)",
      "type": 3,
      "api": "csp_XPathMacFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/vipmv.json"
    },
{"key":"csp_xpath_dxdx","name":"滴嘻滴嘻(XP)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":0,"filterable":1,"ext":"https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/dixidixi.json"},
 {
      "key": "csp_appysv2_独播社",
      "name": "独播社(优)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "http://35ys.cc/api.php/v1.vod"
    },
    {
      "key": "csp_xpath_zx",
      "name": "在线之家(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/%E5%9C%A8%E7%BA%BF%E4%B9%8B%E5%AE%B6.json"
    },
    {
      "key": "csp_appysv2_段友影视",
      "name": "段友影视(优)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "http://121.204.249.135:4433/ruifenglb_api.php/v1.vod"
    },
    {
      "key": "csp_appysv2_1080kk",
      "name": "1080kk(M2)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "http://123.250idc.com/mogai_api.php/v1.vod"
    },
  {
      "key": "csp_appysv2_飓风影院",
      "name": "飓风影院(M2)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "http://yidayy.top/lehailb_api.php/v1.vod"
    },
{
      "key": "csp_appysv2_影视大全",
      "name": "影视大全(M2)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://app.okmedcos.com/ruifenglb_api.php/v1.vod"
    },   
{
      "key": "csp_xpath_zmdy",
      "name": "周末电影(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/zzzlike.json"
    },
    {
      "key": "csp_appysv2_暖光影视",
      "name": "暖光影视(优)",
      "type": 3,
      "api": "csp_AppYsV2",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "https://app.bl210.com/api.php/v1.vod"
    },
   {"key":"csp_xpath_lmys","name":"来米影视(XP)","type":3,"api":"csp_XPathMacFilter","searchable":1,"quickSearch":0,"filterable":1,"ext":"https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/qdytv.json"},
    {
      "key": "csp_xpath_1080p",
      "name": "1080电影(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/1080.json"
    },
{"key":"csp_DY1990","name":"90(SP)","type":3,"api":"csp_DY1990","searchable":1,"quickSearch":1,"filterable":0},
   {
      "key": "2345_spider",
      "name": "2345影院(官源)",
      "type": 3,
      "api": "csp_YS2345",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "http://inews.gtimg.com/newsapp_ls/0/14743078626/0"
    },
    {
      "key": "sogou_spider",
      "name": "搜狗影院(官源)",
      "type": 3,
      "api": "csp_YSSogou",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "http://inews.gtimg.com/newsapp_ls/0/14743078651/0"
    },
    {
      "key": "360_spider",
      "name": "360影院(官源)",
      "type": 3,
      "api": "csp_YS360",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "http://inews.gtimg.com/newsapp_ls/0/14743078668/0"
    },
    {
      "key": "csp_xpath_ikan",
      "name": "爱看(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/ikan.json"
    },
    {
      "key": "csp_xpath_miao",
      "name": "喵喵(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/miaomiao.json"
    },
    {
      "key": "csp_xpath_dym8",
      "name": "电影迷8(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/dianyingmi8.json"
    },
  {
      "key": "csp_xpath_xqm",
      "name": "小强迷(优XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/xiaoqiang.json"
    },
    {
      "key": "csp_xpath_tvby",
      "name": "TVB云播(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/tvb.json"
    },
    {
      "key": "csp_xpath_jubb",
      "name": "剧白白(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/jubaibai.json"
    },
 {
      "key": "csp_xpath_ddt",
      "name": "达达兔(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/%E8%BE%BE%E8%BE%BE%E5%85%94.json"
    },
{
      "key": "csp_xpath_9egy",
      "name": "9E国语(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/%E4%B9%9D%E4%BA%BF%E5%9B%BD%E8%AF%AD.json"
    },
    {
      "key": "csp_xpath_zhangz",
      "name": "厂长资源(XP)",
      "type": 3,
      "api": "csp_XPathFilter",
      "searchable": 0,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "https://yangyang1975.coding.net/p/free/d/mao/git/raw/master/xpath/%E5%8E%82%E9%95%BF%E8%B5%84%E6%BA%90.json"
    },
    {
      "key": "csp_Concert",
      "name": "🎤 演唱会",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "حفلة موسيقية"
    },
    {
      "key": "csp_Animal",
      "name": "🐘 动物世界",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 0,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "عالم الحيوان"
    },
    {
      "key": "csp_Opera",
      "name": "🏮戏曲",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 0,
      "filterable": 1,
      "ext": "أوبرا"
    },
    {
      "key": "csp_360",
      "name": "🎾 360",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 1,
      "quickSearch": 1,
      "filterable": 1,
      "ext": "ثلاثة مائة وستون"
    },
    {
      "key": "csp_FakeWindow",
      "name": "🪟 假窗·氛围·白噪声",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 0,
      "quickSearch": 0,
      "filterable": 0,
      "ext": "نافذة كاذبة"
    },
    {
      "key": "push_agent",
      "name": "🍭 推送",
      "type": 3,
      "api": "csp_ColaCat",
      "searchable": 0,
      "quickSearch": 0,
      "filterable": 0,
      "ext": "علي"
    }
  ],
  "lives": [
    {
      "group": "redirect",
      "channels": [
        {
          "name": "redirect",
          "urls": [
            "proxy://do=tvfix&type=list"
          ]
        }
      ]
    }
  ],
  "parses": [
    {
      "name": "聚合",
      "type": 3,
      "url": "Demo"
    }
  ],
  "flags": [],
  "ijk": [
    {
      "group": "软解码",
      "options": [
        {
          "category": 4,
          "name": "opensles",
          "value": "0"
        },
        {
          "category": 4,
          "name": "overlay-format",
          "value": "842225234"
        },
        {
          "category": 4,
          "name": "framedrop",
          "value": "1"
        },
        {
          "category": 4,
          "name": "soundtouch",
          "value": "1"
        },
        {
          "category": 4,
          "name": "start-on-prepared",
          "value": "1"
        },
        {
          "category": 1,
          "name": "http-detect-range-support",
          "value": "0"
        },
        {
          "category": 1,
          "name": "fflags",
          "value": "fastseek"
        },
        {
          "category": 2,
          "name": "skip_loop_filter",
          "value": "48"
        },
        {
          "category": 4,
          "name": "reconnect",
          "value": "1"
        },
        {
          "category": 4,
          "name": "enable-accurate-seek",
          "value": "0"
        },
        {
          "category": 4,
          "name": "mediacodec",
          "value": "0"
        },
        {
          "category": 4,
          "name": "mediacodec-auto-rotate",
          "value": "0"
        },
        {
          "category": 4,
          "name": "mediacodec-handle-resolution-change",
          "value": "0"
        },
        {
          "category": 4,
          "name": "mediacodec-hevc",
          "value": "0"
        },
        {
          "category": 1,
          "name": "dns_cache_timeout",
          "value": "600000000"
        }
      ]
    },
    {
      "group": "硬解码",
      "options": [
        {
          "category": 4,
          "name": "opensles",
          "value": "0"
        },
        {
          "category": 4,
          "name": "overlay-format",
          "value": "842225234"
        },
        {
          "category": 4,
          "name": "framedrop",
          "value": "1"
        },
        {
          "category": 4,
          "name": "soundtouch",
          "value": "1"
        },
        {
          "category": 4,
          "name": "start-on-prepared",
          "value": "1"
        },
        {
          "category": 1,
          "name": "http-detect-range-support",
          "value": "0"
        },
        {
          "category": 1,
          "name": "fflags",
          "value": "fastseek"
        },
        {
          "category": 2,
          "name": "skip_loop_filter",
          "value": "48"
        },
        {
          "category": 4,
          "name": "reconnect",
          "value": "1"
        },
        {
          "category": 4,
          "name": "enable-accurate-seek",
          "value": "0"
        },
        {
          "category": 4,
          "name": "mediacodec",
          "value": "1"
        },
        {
          "category": 4,
          "name": "mediacodec-auto-rotate",
          "value": "1"
        },
        {
          "category": 4,
          "name": "mediacodec-handle-resolution-change",
          "value": "1"
        },
        {
          "category": 4,
          "name": "mediacodec-hevc",
          "value": "1"
        },
        {
          "category": 1,
          "name": "dns_cache_timeout",
          "value": "600000000"
        }
      ]
    }
  ],
  "ads": [
"mimg.0c1q0l.cn",
"www.googletagmanager.com",
"www.google-analytics.com",
"mc.usihnbcq.cn",
"mg.g1mm3d.cn",
"mscs.svaeuzh.cn",
"cnzz.hhttm.top",
"tp.vinuxhome.com",
"cnzz.mmstat.com",
"www.baihuillq.com",
"s23.cnzz.com",
"z3.cnzz.com",
"c.cnzz.com",
"stj.v1vo.top",
"z12.cnzz.com",
"img.mosflower.cn",
"tips.gamevvip.com",
"ehwe.yhdtns.com",
"xdn.cqqc3.com",
"www.jixunkyy.cn",
"sp.chemacid.cn",
"hm.baidu.com",
"s9.cnzz.com",
"z6.cnzz.com",
"um.cavuc.com",
"mav.mavuz.com",
"wofwk.aoidf3.com",
"z5.cnzz.com",
"xc.hubeijieshikj.cn",
"tj.tianwenhu.com",
"xg.gars57.cn",
"k.jinxiuzhilv.com",
"cdn.bootcss.com",
"ppl.xunzhuo123.com",
"xomk.jiangjunmh.top",
"img.xunzhuo123.com",
"z1.cnzz.com",
"s13.cnzz.com",
"xg.huataisangao.cn",
"z7.cnzz.com",
"xg.huataisangao.cn",
"z2.cnzz.com",
"s96.cnzz.com",
"q11.cnzz.com",
"thy.dacedsfa.cn",
"xg.whsbpw.cn",
"s19.cnzz.com",
"z8.cnzz.com",
"s4.cnzz.com",
"f5w.as12df.top",
"ae01.alicdn.com",
"www.92424.cn",
"k.wudejia.com",
"vivovip.mmszxc.top",
"qiu.xixiqiu.com",
"cdnjs.hnfenxun.com",
"cms.qdwght.com"
],
  "wallpaper": "",
  "spider": "http://static-0feaae6a-16de-44b5-b88c-0025d59ad2c3.bspapp.com/xm.jar"
}